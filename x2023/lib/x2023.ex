defmodule X2023 do
  @moduledoc """
  Documentation for `X2023`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> X2023.hello()
      :world

  """
  def hello do
    :world
  end
end
