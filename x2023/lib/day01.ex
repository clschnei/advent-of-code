defmodule Day01 do
  @digits %{
    zero: "0",
    one: "1",
    two: "2",
    three: "3",
    four: "4",
    five: "5",
    six: "6",
    seven: "7",
    eight: "8",
    nine: "9"
  }

  defp extent(pattern, value) do
    matches =
      Regex.scan(pattern, value)
      |> List.flatten()
      |> Enum.filter(&(&1 != ""))

    [List.first(matches), List.last(matches)]
  end

  defp diagnostic(pattern, value) do
    values = extent(pattern, value)

    values
    |> Enum.map(fn x -> @digits[String.to_atom(x || "zero")] || x end)
    |> Enum.join("")
    |> String.to_integer()
  end

  defp collect(filename, pattern) do
    File.stream!(filename)
    |> Enum.map(&diagnostic(pattern, &1))
    |> Enum.sum()
  end

  def part1(filename) do
    collect(filename, ~r/\d/)
  end

  def part2(filename) do
    collect(filename, ~r/(?=(\d|one|two|three|four|five|six|seven|eight|nine))/)
  end
end
