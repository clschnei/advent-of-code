defmodule Day02 do
  @bag %{
    "red" => 12,
    "green" => 13,
    "blue" => 14
  }

  defp invalid_set(set) do
    cubes = List.first(set)
    color = List.last(set)

    @bag[color] < String.to_integer(cubes)
  end

  defp valid_game([game | sets]) do
    invalid = sets |> Enum.any?(&invalid_set/1)
    gameNumber = List.last(game)

    if invalid, do: nil, else: String.to_integer(gameNumber)
  end

  defp to_entry(string) do
    string |> String.trim() |> String.split(" ")
  end

  defp info(value) do
    value
    |> String.split(~r/:|,|;/)
    |> Enum.map(&to_entry/1)
  end

  defp collect(filename) do
    File.stream!(filename)
    |> Enum.map(&info/1)
  end

  defp game_sum(filename) do
    collect(filename)
    |> Enum.map(&valid_game/1)
    |> Enum.filter(&(&1 != nil))
    |> Enum.sum()
  end

  defp set_power(entries) do
    sets = tl(entries)

    power =
      sets
      |> Enum.group_by(&List.last/1)
      |> Enum.map(fn {_, values} ->
        values |> Enum.map(&List.first/1) |> Enum.map(&String.to_integer/1) |> Enum.max()
      end)
      |> Enum.reduce(1, fn a, b -> a * b end)

    power
  end

  defp game_power_sum(filename) do
    collect(filename)
    |> Enum.map(&set_power/1)
    |> Enum.sum()
  end

  def part1(filename) do
    game_sum(filename)
  end

  def part2(filename) do
    game_power_sum(filename)
  end
end
