defmodule Day02Test do
  use ExUnit.Case

  @input "./input/day02.txt"

  test "part 1" do
    assert Day02.part1(@input) == 2486
  end

  test "part 2" do
    assert Day02.part2(@input) == 87984
  end
end
