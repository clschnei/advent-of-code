defmodule Day01Test do
  use ExUnit.Case

  @input "./input/day01.txt"

  test "part 1" do
    assert Day01.part1(@input) == 54159
  end

  test "part 2" do
    assert Day01.part2(@input) == 53866
  end
end
